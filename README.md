Python Uploader
===============
This program is meant to take a hex file stored on your computer and upload it to an arduino mega

Creating an install
-------------------
This program is written in python and most computers do not support python out of the box. In this circumstance a standalone compiler must be made for these users.  
In order to do this we use a program called pyinstaller. This can be found at pyinstaller.org.  

This function will run python and the python installer program on the main.py file  
`C:\Python27\python.exe C:\<PATH TO EXTRACTED DIRECTORY>\pyinstaller-2.0\pyinstaller.py main.py`  

An example of the above command with the directory in place is
`C:\Python27\python.exe C:\User\admin\Desktop\pyinstaller.py main.py`

After you do this a new directory called `dist` should appear. Copy three files into `dist/main` in order to finish creating the stand alone program
`avrdude.conf` a configuration file on how to upload to the arduino  
`avrdude.exe` avrdude to upload the program to the arduino  
`libusb0.dll` used by avrdude for acessing the USB  

Lastly include the `upload.hex` file in the directory and distribute that directory as a standalone application


How to use the Hex Uploader
---------------------------

In this package all the files you need to run the program are located in the folder "Bundle". In order to run the program all you need to do is double click on the main.exe program to run it. Once it is running type in the com port that you wish to upload to and hit enter. AVRdude should begin running and upload your program from your computer to the microchip.

If you wish to change which file gets uploaded simply replace upload.hex with the hex file of your choice.

File Package Description
------------------------

_hashlib.pyd .................. Library file created by pyinstaller
avrdude.conf .................. the arduino configuration file for avrdude
avrdude.exe ................... avrdude executable to allow for uploading
bz2.pyd  ...................... library file created by pyinstaller
libusb0.dll ................... usb communication library for avrdude
main.exe ...................... the main program for hex uploader
main.exe.manifest ............. Program Manifest file created by pyinstaller
Microsoft.VC90.CRT.manifest ... Program manifest file created by pyinstaller
msvcm90.dll ................... Library file created by pyinstaller
msvcp90.dll ................... Library file created by pyinstaller
msvcr90.dll ................... Library file created by pyinstaller
python27.dll .................. Library file created by pyinstaller
delect.pyd .................... Library file created by pyinstaller
unicodedata.pyd ............... Library file created by pyinstaller
upload.hex .................... The hex file that is being uploaded to the avr chip
