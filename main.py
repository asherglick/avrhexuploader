# 
import subprocess, sys
from PyQt4 import QtGui, QtCore

class mainWidget(QtGui.QWidget):
	def __init__(self):
		super(mainWidget, self).__init__()
		#self.setStyleSheet("*{margin-left: auto;margin-right: auto}")
		#self.setStyleSheet("*{margin-left: 10px;}")
		self.setStyleSheet("""
			QPushButton {
				width:400px;
				height: 30px;
			}
			QLineEdit {
				height: 30px;
			}
			QLabel {
				margin-top: 9px;
				margin-bottom: 6px;
			}
			QComboBox {
				height: 30px;
			}
			""")
		self.initUI()
		self.isPasswordCopied = False


	def initUI(self):
		#self.resize(70, 70)
		self.setWindowTitle("Hex uploader")

		self.inputhlayout = QtGui.QHBoxLayout()
		self.inputhlayout.addStretch(1)
		self.inputvlayout = QtGui.QVBoxLayout()
		self.inputvlayout.addStretch(1)
		
		# Create the filename text box
		self.filename = QtGui.QLineEdit("",self)
		self.filename.setPlaceholderText("File To Upload")
		self.filename.setText("upload.hex")
		self.inputvlayout.addWidget(self.filename)
		# Create the show password button
		self.displayPasswordButton = QtGui.QPushButton("Change Hex File", self)
		self.displayPasswordButton.clicked.connect(self.searchForFile)
		self.inputvlayout.addWidget(self.displayPasswordButton)
		# Create the port text box

		self.comboPort = QtGui.QComboBox()
		#self.comboPort.addItem("--CHOOSE A PORT--")
		for i in enumerate_serial_ports():
			self.comboPort.addItem(i[0])
		self.inputvlayout.addWidget(self.comboPort)


		#self.port = QtGui.QLineEdit("",self)
		#self.port.setPlaceholderText("Port")
		#self.inputvlayout.addWidget(self.port)

		self.uploadButton = QtGui.QPushButton("Upload", self)
		self.uploadButton.clicked.connect(self.upload)
		self.inputvlayout.addWidget(self.uploadButton)
		# Finish configuring the layput
		self.inputvlayout.addStretch(1)
		self.inputhlayout.addLayout(self.inputvlayout)
		self.inputhlayout.addStretch(1)
		# Set the layout to the hlayout
		self.setLayout(self.inputhlayout)

	def searchForFile(self):
		filename = QtGui.QFileDialog.getOpenFileName(self, "Open File", "", "")
		if filename != "":
			self.filename.setText(filename)
	
	def upload(self):
		comPort = str(self.comboPort.currentText())
		hexPath = str(self.filename.text())

		#print "What serial port do you want to upload to?"
		#comPort = raw_input()
	
		print "You are uploading to the port", comPort
		#boringdelay = raw_input()

		# full command
		# avrdude.exe -Cavrdude.conf -v -v -v -v -patmega2560 -cwiring -P\\.\COM4 -b115200 -D -Uflash:w:Blink.cpp.hex:i

		avrdudePath = "avrdude.exe"
		configurationPath = "avrdude.conf"
		processor = "atmega2560"
		#port = "\\\\.\\COM" + comPort
		port = "\\\\.\\"+comPort
		protocol = "wiring"
		baudrate = "115200"
		#hexPath = "upload.hex"

		# call avrdude to upload .hex
		subprocess.call([
		    avrdudePath,
		    '-C', configurationPath,
		    '-p', processor,
		    '-P', port,
		    '-c', protocol,
		    '-b', baudrate,
		    '-D',
		    '-U', 'flash:w:%s:i' % hexPath,
		])


#boringdelay = input()

comPort=""
hexPath=""
# call the main widget
def main():

	app = QtGui.QApplication(sys.argv)

	gui = mainWidget()
	gui.show()

	app.exec_()
	#print "Exiting"


	

	exit()


import _winreg as winreg
import itertools


def enumerate_serial_ports():
    """ Uses the Win32 registry to return a iterator of serial 
        (COM) ports existing on this computer.


    """
    path = 'HARDWARE\\DEVICEMAP\\SERIALCOMM'
    try:
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path)
    except WindowsError:
        raise IterationError

    for i in itertools.count():
        try:
            val = winreg.EnumValue(key, i)
            yield (str(val[1]), str(val[0]))
        except EnvironmentError:
            break

# run the main function
if __name__ == '__main__':
	#
	main()